package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_alterInfo;

public class HauptActivity extends AppCompatActivity implements View.OnClickListener {

    public static Button btn_onlineMode;
    Button btn_filter;
    Button btn_credits;
    Button btn_list;
    Button btn_alertDialog;

    private static final int REQUEST_ACCESS_FINE_LOCATION = 111, REQUEST_WRITE_STORAGE = 112;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haupt);
        setTitle("W'n'T Finder");

        //disable first, gets enabled if in update Button in Filter was clicked
        btn_onlineMode = (Button) findViewById(R.id.btn_onlineMode);
        if (btn_onlineMode != null) {
            btn_onlineMode.setOnClickListener(this);
        }
        if (btn_onlineMode != null) {
            btn_onlineMode.setEnabled(false);
        }

        btn_filter = (Button) findViewById(R.id.btn_filter);
        if (btn_filter != null) {
            btn_filter.setOnClickListener(this);
        }

        btn_list = (Button) findViewById(R.id.btn_list);
        if (btn_list != null) {
            btn_list.setOnClickListener(this);
        }

        btn_credits = (Button) findViewById(R.id.btn_credits);
        if (btn_credits != null) {
            btn_credits.setOnClickListener(this);
        }

        btn_alertDialog = (Button) findViewById(R.id.btn_alertDialog);
        if (btn_alertDialog != null) {
            btn_alertDialog.setOnClickListener(this);
        }

        //Test ACCESS_FINE_LOCATION
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        Log.d("WRITE_EXTERNAL_STORAGE", String.valueOf(permissionCheck));

        boolean hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionLocation) {
            ActivityCompat.requestPermissions(HauptActivity.this,
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        }

        boolean hasPermissionWrite = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionWrite) {
            ActivityCompat.requestPermissions(HauptActivity.this,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }
    public void open(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage((getResources().getString(str_alterInfo)));
                alertDialogBuilder.setPositiveButton("got it!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg1, int arg0) {
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(HauptActivity.this, "Permissions granted!", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                } else {
                    Toast.makeText(HauptActivity.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(HauptActivity.this, "Permissions granted!", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                } else {
                    Toast.makeText(HauptActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
            }
            default:
                break;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_alertDialog:
                open(v);
                break;
            case R.id.btn_onlineMode:
                Configuration config = getResources().getConfiguration();
                if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    Intent intentOnlineLM = new Intent(this, OnlineActivityLM.class);
                    startActivity(intentOnlineLM);
                }
                else {
                    Intent intentOnlinePM = new Intent(this, OnlineActivityPM.class);
                    startActivity(intentOnlinePM);
                }
                break;

            case R.id.btn_filter:
                Intent intentFilter = new Intent(this, FilterFragment.class);
                startActivity(intentFilter);
                break;

            case  R.id.btn_list:
                Intent intentList = new Intent(this, JsonParser.class);
                startActivity(intentList);
                break;

            case R.id.btn_credits:
                Intent intentCredits = new Intent(this, CreditsActivity.class);
                startActivity(intentCredits);
                break;
            default:
                break;
        }
    }
} //end of Activity