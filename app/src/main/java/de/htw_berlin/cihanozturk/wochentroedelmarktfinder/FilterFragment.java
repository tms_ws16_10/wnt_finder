package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;

public class FilterFragment extends Activity {
    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        /**
         * Check the device orientation and act accordingly
         */
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            /**
             * Landscape mode of the device
             */
            LMFragment lsFragment = new LMFragment();
            fragmentTransaction.replace(android.R.id.content, lsFragment);
        } else {
            /**
             * Portrait mode of the device
             */
            PMFragement pmFragment = new PMFragement();
            fragmentTransaction.replace(android.R.id.content, pmFragment);
        }
        fragmentTransaction.commit();
    }
} //end of class