package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class CreditsActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        setTitle("Credits");
    }
} //end of Activity