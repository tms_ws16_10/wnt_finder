package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import java.util.ArrayList;

import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_bBurg;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_cBurgWdorf;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_fHainKberg;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_klaistow;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_lBerg;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_mZahnHdorf;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_mitte;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_nKoelln;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_pankow;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_rDorf;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_sLitzZdorf;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_spandau;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_tHofSberg;
import static de.htw_berlin.cihanozturk.wochentroedelmarktfinder.R.string.str_tTowKnick;

public class OnlineActivityLM extends AppCompatActivity implements LocationListener, View.OnClickListener {
    private LocationManager locationManger;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 111;
    private MapView mMapView;
    private MapController mc;
    ArrayList<OverlayItem> overlayItemArrayBburg;
    ArrayList<OverlayItem> overlayItemArrayCburWdorf;
    ArrayList<OverlayItem> overlayItemArrayFhainKberg;
    ArrayList<OverlayItem> overlayItemArrayKlaistow;
    ArrayList<OverlayItem> overlayItemArrayLberg;
    ArrayList<OverlayItem> overlayItemArrayMzahnHdorf;
    ArrayList<OverlayItem> overlayItemArrayMitte;
    ArrayList<OverlayItem> overlayItemArrayNkoelln;
    ArrayList<OverlayItem> overlayItemArrayPankow;
    ArrayList<OverlayItem> overlayItemArrayRdorf;
    ArrayList<OverlayItem> overlayItemArraySpandau;
    ArrayList<OverlayItem> overlayItemArraySlitzZdorf;
    ArrayList<OverlayItem> overlayItemArrayThofSberg;
    ArrayList<OverlayItem> overlayItemArrayTtowKnick;


    //is district selected?
    private Boolean bBChecked = false;
    private Boolean cWChecked = false;
    private Boolean fKChecked = false;
    private Boolean kChecked = false;
    private Boolean lChecked = false;
    private Boolean mHChecked = false;
    private Boolean mChecked = false;
    private Boolean nKChecked = false;
    private Boolean pChecked = false;
    private Boolean rDChecked = false;
    private Boolean sChecked = false;
    private Boolean sZChecked = false;
    private Boolean tSChecked = false;
    private Boolean tKChecked = false;

    protected void onCreate(Bundle savedInstanceState) {
        new OnlineActivityLM.GetItems().execute();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online);
        setTitle("Map");
        org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants.setUserAgentValue(BuildConfig.APPLICATION_ID);

        mMapView = (MapView) findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.setTileSource(TileSourceFactory.MAPNIK);
        }

        if (mMapView != null) {
            mMapView.setBuiltInZoomControls(true);
        }
        if (mMapView != null) {
            mMapView.setMultiTouchControls(true);
        }

        if (mMapView != null) {
            mc = (MapController) mMapView.getController();
        }
        final int zoom = 11;
        mc.setZoom(zoom);
        final double berlinLat = 52.520007;
        final double berlinLng = 13.404954;
        GeoPoint geoPoint = new GeoPoint(berlinLat, berlinLng);
        mc.animateTo(geoPoint);

        overlayItemArrayBburg = new ArrayList<>();
        overlayItemArrayCburWdorf = new ArrayList<>();
        overlayItemArrayFhainKberg = new ArrayList<>();
        overlayItemArrayKlaistow = new ArrayList<>();
        overlayItemArrayLberg = new ArrayList<>();
        overlayItemArrayMzahnHdorf = new ArrayList<>();
        overlayItemArrayMitte = new ArrayList<>();
        overlayItemArrayNkoelln = new ArrayList<>();
        overlayItemArrayPankow = new ArrayList<>();
        overlayItemArrayRdorf = new ArrayList<>();
        overlayItemArraySpandau = new ArrayList<>();
        overlayItemArraySlitzZdorf = new ArrayList<>();
        overlayItemArrayThofSberg = new ArrayList<>();
        overlayItemArrayTtowKnick = new ArrayList<>();

        //// Add the Array to the IconOverlay
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay1 = new ItemizedIconOverlay<>(this, overlayItemArrayBburg, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay2 = new ItemizedIconOverlay<>(this, overlayItemArrayCburWdorf, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay3 = new ItemizedIconOverlay<>(this, overlayItemArrayFhainKberg, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay4 = new ItemizedIconOverlay<>(this, overlayItemArrayKlaistow, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay5 = new ItemizedIconOverlay<>(this, overlayItemArrayLberg, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay6 = new ItemizedIconOverlay<>(this, overlayItemArrayMzahnHdorf, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay7 = new ItemizedIconOverlay<>(this, overlayItemArrayMitte, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay8 = new ItemizedIconOverlay<>(this, overlayItemArrayNkoelln, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay9 = new ItemizedIconOverlay<>(this, overlayItemArrayPankow, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay10 = new ItemizedIconOverlay<>(this, overlayItemArrayRdorf, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay11 = new ItemizedIconOverlay<>(this, overlayItemArraySpandau, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay12 = new ItemizedIconOverlay<>(this, overlayItemArraySlitzZdorf, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay13 = new ItemizedIconOverlay<>(this, overlayItemArrayThofSberg, null);
        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay14 = new ItemizedIconOverlay<>(this, overlayItemArrayTtowKnick, null);

        // Add the overlay to the MapView
        mMapView.getOverlays().add(itemizedIconOverlay1);
        mMapView.getOverlays().add(itemizedIconOverlay2);
        mMapView.getOverlays().add(itemizedIconOverlay3);
        mMapView.getOverlays().add(itemizedIconOverlay4);
        mMapView.getOverlays().add(itemizedIconOverlay5);
        mMapView.getOverlays().add(itemizedIconOverlay6);
        mMapView.getOverlays().add(itemizedIconOverlay7);
        mMapView.getOverlays().add(itemizedIconOverlay8);
        mMapView.getOverlays().add(itemizedIconOverlay9);
        mMapView.getOverlays().add(itemizedIconOverlay10);
        mMapView.getOverlays().add(itemizedIconOverlay11);
        mMapView.getOverlays().add(itemizedIconOverlay12);
        mMapView.getOverlays().add(itemizedIconOverlay13);
        mMapView.getOverlays().add(itemizedIconOverlay14);

        Button btn_gps = (Button) findViewById(R.id.btn_gps);
        if (btn_gps != null) {
            btn_gps.setOnClickListener(this);
        }

        //show only the selected districts referring to LMFragment (checkboxes)
        //check if user comes from LMFragment
        //for LM
        if (LMFragment.comesFromFilter) {
            //check if districts are selected or not
            bBChecked = LMFragment.bBurg.isChecked();
            cWChecked = LMFragment.cBurgWilmers.isChecked();
            fKChecked = LMFragment.fHainKberg.isChecked();
            kChecked = LMFragment.klaistow.isChecked();
            lChecked = LMFragment.lBerg.isChecked();
            mHChecked = LMFragment.mZahnHdorf.isChecked();
            mChecked = LMFragment.mitte.isChecked();
            nKChecked = LMFragment.nKoelln.isChecked();
            pChecked = LMFragment.pankow.isChecked();
            rDChecked = LMFragment.rDorf.isChecked();
            sChecked = LMFragment.spandau.isChecked();
            sZChecked = LMFragment.sLitzZdorf.isChecked();
            tSChecked = LMFragment.tHofSberg.isChecked();
            tKChecked = LMFragment.tTowKnick.isChecked();
        }
    }

    public void addMarket(GeoPoint center) {
        Marker marker = new Marker(mMapView);
        marker.setPosition(center);
        //set icon for marker
        Drawable icon = getDrawable(R.drawable.person);
        //Set the bounding for the drawable
        if (icon != null) {
            icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
        }
        //Set the marker to the overlay
        marker.setIcon(icon);
        mMapView.getOverlays().add(marker);
        mMapView.invalidate();
    }

    public void onLocationChanged(Location location) {
        //final double latitude=52.520007;
        //final double longitude=13.404954;
        GeoPoint center = new GeoPoint(location.getLatitude(), location.getLongitude());
        mc.animateTo(center);
        final int zoom2 = 20;
        mc.setZoom(zoom2);
        addMarket(center);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) { }

    public void onProviderEnabled(String provider) { }

    public void onProviderDisabled(String provider) { }

    public void onDestroy() {
        super.onDestroy();
        if (locationManger != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManger.removeUpdates(this);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_gps:
                boolean hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
                if (!hasPermissionLocation) {
                    ActivityCompat.requestPermissions(OnlineActivityLM.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ACCESS_FINE_LOCATION);
                }
                locationManger = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                final int rLocUpd = 10000;
                locationManger.requestLocationUpdates(LocationManager.GPS_PROVIDER, rLocUpd, 0, this);
                break;
            default:
                break;
        }
    }

    private String TAG = OnlineActivityLM.class.getSimpleName();

    private class GetItems extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(OnlineActivityLM.this, "Json Data is downloading", Toast.LENGTH_LONG).show();
        }

        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/wochen-troedelmaerkte/index.php/index/all.json?q=";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray index = jsonObj.getJSONArray("index");

                    // looping through All items
                    // create markers for all districts
                    for (int i = 0; i < index.length(); i++) {

                        JSONObject c = index.getJSONObject(i);
                        String id = c.getString("id");
                        String bezirk = c.getString("bezirk");
                        String latitude = c.getString("latitude");
                        String longitude = c.getString("longitude");

                        //note: double district names are divided with "-" in json
                        if (bezirk.equals((getResources().getString(str_bBurg)))) {
                            if (bBChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem bBurgItem = new OverlayItem((getResources().getString(str_bBurg)), "Berlin",
                                        new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker1);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                bBurgItem.setMarker(icon);
                                overlayItemArrayBburg.add(bBurgItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_cBurgWdorf)))) {
                            if (cWChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set marker
                                OverlayItem cBurgWdorfItem = new OverlayItem((getResources().getString(str_cBurgWdorf)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker2);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                cBurgWdorfItem.setMarker(icon);
                                overlayItemArrayCburWdorf.add(cBurgWdorfItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_fHainKberg)))) {
                            if (fKChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem fHainKbergItem = new OverlayItem((getResources().getString(str_fHainKberg)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker3);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                fHainKbergItem.setMarker(icon);
                                overlayItemArrayFhainKberg.add(fHainKbergItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_klaistow)))) {
                            if (kChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem klaistowItem = new OverlayItem((getResources().getString(str_klaistow)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker4);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                klaistowItem.setMarker(icon);
                                overlayItemArrayKlaistow.add(klaistowItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_lBerg)))) {
                            if (lChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem lBergItem = new OverlayItem((getResources().getString(str_lBerg)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker5);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                lBergItem.setMarker(icon);
                                overlayItemArrayLberg.add(lBergItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_mZahnHdorf)))) {
                            if (mHChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem mZahnHdorfItem = new OverlayItem((getResources().getString(str_mZahnHdorf)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker6);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                mZahnHdorfItem.setMarker(icon);
                                overlayItemArrayMzahnHdorf.add(mZahnHdorfItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_mitte)))) {
                            if (mChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem mitteItem = new OverlayItem((getResources().getString(str_mitte)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker7);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                mitteItem.setMarker(icon);
                                overlayItemArrayMitte.add(mitteItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_nKoelln)))) {
                            if (nKChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem nKoellnItem = new OverlayItem((getResources().getString(str_nKoelln)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker8);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                nKoellnItem.setMarker(icon);
                                overlayItemArrayNkoelln.add(nKoellnItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_rDorf)))) {
                            if (rDChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem rDorfItem = new OverlayItem((getResources().getString(str_rDorf)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker9);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                rDorfItem.setMarker(icon);
                                overlayItemArrayRdorf.add(rDorfItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_pankow)))) {
                            if (pChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem pankowItem = new OverlayItem((getResources().getString(str_pankow)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker10);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                pankowItem.setMarker(icon);
                                overlayItemArrayPankow.add(pankowItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_spandau)))) {
                            if (sChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem spandauItem = new OverlayItem((getResources().getString(str_spandau)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker11);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                spandauItem.setMarker(icon);
                                overlayItemArraySpandau.add(spandauItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_sLitzZdorf)))) {
                            if (sZChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem sLitzZdorfItem = new OverlayItem((getResources().getString(str_sLitzZdorf)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker12);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                sLitzZdorfItem.setMarker(icon);
                                overlayItemArraySlitzZdorf.add(sLitzZdorfItem);
                            }
                        }

                        //1 markt has no values for "latitude" and "longitude" in Json
                        else if (bezirk.equals((getResources().getString(str_tHofSberg))) && !id.equals("303")) {
                            if (tSChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem tHofSbergItem = new OverlayItem((getResources().getString(str_tHofSberg)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker13);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                tHofSbergItem.setMarker(icon);
                                overlayItemArrayThofSberg.add(tHofSbergItem);
                            }
                        }

                        else if (bezirk.equals((getResources().getString(str_tTowKnick)))) {
                            if (tKChecked) {
                                //replace "," with "." in "latitude" and "longitude" for correct parsing
                                String lati = latitude.replaceAll(",", ".");
                                Double lat = Double.parseDouble(lati);
                                String lngi = longitude.replaceAll(",", ".");
                                Double lng = Double.parseDouble(lngi);
                                //set markers
                                OverlayItem tTowKnickItem = new OverlayItem((getResources().getString(str_tTowKnick)), "Berlin", new GeoPoint(lat, lng));
                                //set icon for marker
                                Drawable icon = getDrawable(R.drawable.marker14);
                                //Set the bounding for the drawable
                                if (icon != null) {
                                    icon.setBounds(0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), icon.getIntrinsicWidth() / 2, 0);
                                }
                                //Set the marker to the overlay
                                tTowKnickItem.setMarker(icon);
                                overlayItemArrayTtowKnick.add(tTowKnickItem);
                            }
                        }
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            //// Add the Array to the IconOverlay
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay1 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayBburg, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay2 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayCburWdorf, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay3 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayFhainKberg, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay4 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayKlaistow, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay5 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayLberg, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay6 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayMzahnHdorf, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay7 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayMitte, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay8 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayNkoelln, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay9 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayPankow, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay10 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayRdorf, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay11 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArraySpandau, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay12 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArraySlitzZdorf, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay13 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayThofSberg, null);
            ItemizedIconOverlay<OverlayItem> itemItemizedIconOverlay14 = new ItemizedIconOverlay<>(OnlineActivityLM.this, overlayItemArrayTtowKnick, null);

            // Add the overlay to the MapView
            mMapView.getOverlays().add(itemItemizedIconOverlay1);
            mMapView.getOverlays().add(itemItemizedIconOverlay2);
            mMapView.getOverlays().add(itemItemizedIconOverlay3);
            mMapView.getOverlays().add(itemItemizedIconOverlay4);
            mMapView.getOverlays().add(itemItemizedIconOverlay5);
            mMapView.getOverlays().add(itemItemizedIconOverlay6);
            mMapView.getOverlays().add(itemItemizedIconOverlay7);
            mMapView.getOverlays().add(itemItemizedIconOverlay8);
            mMapView.getOverlays().add(itemItemizedIconOverlay9);
            mMapView.getOverlays().add(itemItemizedIconOverlay10);
            mMapView.getOverlays().add(itemItemizedIconOverlay11);
            mMapView.getOverlays().add(itemItemizedIconOverlay12);
            mMapView.getOverlays().add(itemItemizedIconOverlay13);
            mMapView.getOverlays().add(itemItemizedIconOverlay14);
        }
    }
} //end of Class