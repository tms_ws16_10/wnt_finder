package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class JsonParser extends AppCompatActivity {
    private String TAG = JsonParser.class.getSimpleName();
    private ListView lv;

    //ArrayList for bezirke
    ArrayList<HashMap<String, String>> itemList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsonparser);
        setTitle("List");
        itemList = new ArrayList<>();
        lv = (ListView) findViewById(R.id.btn_list);
        new GetItems().execute();
    }

    private class GetItems extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(JsonParser.this, "Json Data is downloading", Toast.LENGTH_LONG).show();
        }

        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/wochen-troedelmaerkte/index.php/index/all.json?q=";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray index = jsonObj.getJSONArray("index");

                    // looping through All items
                    for (int i = 0; i < index.length(); i++) {
                        JSONObject c = index.getJSONObject(i);
                        String id = c.getString("id");
                        String bezirk = c.getString("bezirk");
                        String location = c.getString("location");
                        String latitude = c.getString("latitude");
                        String longitude = c.getString("longitude");
                        String tage = c.getString("tage");
                        String zeiten = c.getString("zeiten");
                        String betreiber = c.getString("betreiber");
                        String email = c.getString("email");
                        String web = c.getString("www");
                        String bemerkungen = c.getString("bemerkungen");

                        // tmp hash map for single items
                        //note: double bezirke names are divided with "-" in json
                        HashMap<String, String> items = new HashMap<>();

                        // adding each child node to HashMap key => value
                        //all bezirke
                        items.put("id", id);
                        items.put("bezirk", bezirk);
                        items.put("location", location);
                        items.put("latitude", latitude);
                        items.put("longitude", longitude);
                        items.put("tage", tage);
                        items.put("zeiten", zeiten);
                        items.put("betreiber", betreiber);
                        items.put("email", email);
                        items.put("www", web);
                        items.put("bemerkungen", bemerkungen);

                        // adding item to itemLists
                        //for filterActivity
                        itemList.add(items);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ListAdapter adapter = new SimpleAdapter(JsonParser.this, itemList,
                    R.layout.list_item, new String[] {"bezirk", "location", "tage", "zeiten", "www"},
                    new int[]{R.id.bezirk, R.id.adresse, R.id.tage, R.id.zeiten, R.id.web });
            lv.setAdapter(adapter);
        }
    } //end of GetItems
} //end of class