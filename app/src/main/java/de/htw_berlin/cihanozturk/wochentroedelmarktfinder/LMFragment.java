package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class LMFragment extends Fragment implements View.OnClickListener {
    public static CheckBox bBurg;
    public static CheckBox cBurgWilmers;
    public static CheckBox fHainKberg;
    public static CheckBox klaistow;
    public static CheckBox lBerg;
    public static CheckBox mZahnHdorf;
    public static CheckBox mitte;
    public static CheckBox nKoelln;
    public static CheckBox pankow;
    public static CheckBox rDorf;
    public static CheckBox spandau;
    public static CheckBox sLitzZdorf;
    public static CheckBox tHofSberg;
    public static CheckBox tTowKnick;
    public static CheckBox allD;

    //check for other Activities
    public static Boolean comesFromFilter;

    public static Boolean bBurgChecked;
    public static Boolean cBurgWilmersChecked;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         * Inflate the layout for this fragment, then return the View
         */
        View layout = inflater.inflate(R.layout.lm_fragment, container, false);

        comesFromFilter = false;
        //set CheckboxesSelected to false
        bBurgChecked = false;
        cBurgWilmersChecked = false;

        //Button
        Button update = (Button) layout.findViewById(R.id.btn_aktualisieren);
        update.setOnClickListener(this);

        //Checkboxes
        bBurg = (CheckBox) layout.findViewById(R.id.bBurg);
        bBurg.setOnClickListener(this);

        cBurgWilmers = (CheckBox) layout.findViewById(R.id.cBurgWilmers);
        cBurgWilmers.setOnClickListener(this);

        fHainKberg = (CheckBox) layout.findViewById(R.id.fHainKberg);
        fHainKberg.setOnClickListener(this);

        klaistow = (CheckBox) layout.findViewById(R.id.klaistow);
        klaistow.setOnClickListener(this);

        lBerg = (CheckBox) layout.findViewById(R.id.lBerg);
        lBerg.setOnClickListener(this);

        mZahnHdorf = (CheckBox) layout.findViewById(R.id.mZahnHdorf);
        mZahnHdorf.setOnClickListener(this);

        mitte = (CheckBox) layout.findViewById(R.id.mitte);
        mitte.setOnClickListener(this);

        nKoelln = (CheckBox) layout.findViewById(R.id.nKölln);
        nKoelln.setOnClickListener(this);

        pankow = (CheckBox) layout.findViewById(R.id.pankow);
        pankow.setOnClickListener(this);

        rDorf = (CheckBox) layout.findViewById(R.id.rDorf);
        rDorf.setOnClickListener(this);

        spandau = (CheckBox) layout.findViewById(R.id.spandau);
        spandau.setOnClickListener(this);

        sLitzZdorf = (CheckBox) layout.findViewById(R.id.sLitzZdorf);
        sLitzZdorf.setOnClickListener(this);

        tHofSberg = (CheckBox) layout.findViewById(R.id.tHofSberg);
        tHofSberg.setOnClickListener(this);

        tTowKnick = (CheckBox) layout.findViewById(R.id.tTowKnick);
        tTowKnick.setOnClickListener(this);

        allD = (CheckBox) layout.findViewById(R.id.allD);
        allD.setOnClickListener(this);

        //default districts
        bBurg.setChecked(true);
        cBurgWilmers.setChecked(true);
        rDorf.setChecked(true);
        return layout;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bBurg:
                Toast.makeText(getActivity() , "Brandenburg checkbox checked", Toast.LENGTH_SHORT).show();
                if (bBurg.isChecked()) bBurg.setChecked(true);
                break;
            case R.id.cBurgWilmers:
                Toast.makeText(getActivity() , "Charlottenburg-Wilmersdorf checkbox checked", Toast.LENGTH_SHORT).show();
                if (cBurgWilmers.isChecked()) cBurgWilmers.setChecked(true);
                break;
            case R.id.fHainKberg:
                Toast.makeText(getActivity() , "Friedrichshain-Kreuzberg checkbox checked", Toast.LENGTH_SHORT).show();
                if (fHainKberg.isChecked()) fHainKberg.setChecked(true);
                break;
            case R.id.klaistow:
                Toast.makeText(getActivity() , "Klaistow checkbox checked", Toast.LENGTH_SHORT).show();
                if (klaistow.isChecked()) klaistow.setChecked(true);
                break;
            case R.id.lBerg:
                Toast.makeText(getActivity() , "Lichtenberg checkbox checked", Toast.LENGTH_SHORT).show();
                if (lBerg.isChecked()) lBerg.setChecked(true);
                break;
            case R.id.mZahnHdorf:
                Toast.makeText(getActivity() , "Marzahn-Hellersdorf checkbox checked", Toast.LENGTH_SHORT).show();
                if (mZahnHdorf.isChecked()) mZahnHdorf.setChecked(true);
                break;
            case R.id.mitte:
                Toast.makeText(getActivity() , "Mitte checkbox checked", Toast.LENGTH_SHORT).show();
                if (mitte.isChecked()) mitte.setChecked(true);
                break;
            case R.id.nKölln:
                Toast.makeText(getActivity() , "Neukoelln checkbox checked", Toast.LENGTH_SHORT).show();
                if (nKoelln.isChecked()) nKoelln.setChecked(true);
                break;
            case R.id.pankow:
                Toast.makeText(getActivity() , "Pankow checkbox checked", Toast.LENGTH_SHORT).show();
                if (pankow.isChecked()) pankow.setChecked(true);
                break;
            case R.id.rDorf:
                Toast.makeText(getActivity() , "Reinickendorf checkbox checked", Toast.LENGTH_SHORT).show();
                if (rDorf.isChecked()) rDorf.setChecked(true);
                break;
            case R.id.spandau:
                Toast.makeText(getActivity() , "Spandau checkbox checked", Toast.LENGTH_SHORT).show();
                if (spandau.isChecked()) spandau.setChecked(true);
                break;
            case R.id.sLitzZdorf:
                Toast.makeText(getActivity() , "Steglitz-Zehlendorf checkbox checked", Toast.LENGTH_SHORT).show();
                if (sLitzZdorf.isChecked()) sLitzZdorf.setChecked(true);
                break;
            case R.id.tHofSberg:
                Toast.makeText(getActivity() , "Tempelhof-Schoeneberg checkbox checked", Toast.LENGTH_SHORT).show();
                if (tHofSberg.isChecked()) tHofSberg.setChecked(true);
                break;
            case R.id.tTowKnick:
                Toast.makeText(getActivity() , "Treptow-Koepenick checkbox checked", Toast.LENGTH_SHORT).show();
                if (tTowKnick.isChecked()) tTowKnick.setChecked(true);
                break;

            case R.id.allD:
                Toast.makeText(getActivity() , "allD checkbox checked", Toast.LENGTH_SHORT).show();
                if (allD.isChecked()) {
                    allD.setChecked(true);
                    //set all to clicked
                    tTowKnick.setChecked(true);
                    tHofSberg.setChecked(true);
                    sLitzZdorf.setChecked(true);
                    spandau.setChecked(true);
                    rDorf.setChecked(true);
                    pankow.setChecked(true);
                    nKoelln.setChecked(true);
                    mitte.setChecked(true);
                    mZahnHdorf.setChecked(true);
                    lBerg.setChecked(true);
                    klaistow.setChecked(true);
                    fHainKberg.setChecked(true);
                    cBurgWilmers.setChecked(true);
                    bBurg.setChecked(true);
                }
                else {
                    allD.setChecked(false);
                    tTowKnick.setChecked(false);
                    tHofSberg.setChecked(false);
                    sLitzZdorf.setChecked(false);
                    spandau.setChecked(false);
                    rDorf.setChecked(false);
                    pankow.setChecked(false);
                    nKoelln.setChecked(false);
                    mitte.setChecked(false);
                    mZahnHdorf.setChecked(false);
                    lBerg.setChecked(false);
                    klaistow.setChecked(false);
                    fHainKberg.setChecked(false);
                    cBurgWilmers.setChecked(false);
                    bBurg.setChecked(false);
                }
                break;

            case R.id.btn_aktualisieren:
                HauptActivity.btn_onlineMode.setEnabled(true);
                comesFromFilter = true;
                Intent intentLM = new Intent(getActivity(), OnlineActivityLM.class);
                startActivity(intentLM);
                break;

            default:
                break;
        }
    }
} //end of class