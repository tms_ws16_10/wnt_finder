package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.content.pm.ActivityInfo;
import android.test.ActivityInstrumentationTestCase2;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

public class LMFragmentTest extends ActivityInstrumentationTestCase2<FilterFragment> {
    public LMFragmentTest() {
        super(FilterFragment.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testDefaultDistrictsAreChecked() {
        //turn into landscape
        //check if default districts are checked
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.bBurg)).check(matches(isChecked()));
        onView(withId(R.id.cBurgWilmers)).check(matches(isChecked()));
        onView(withId(R.id.rDorf)).check(matches(isChecked()));
        //check random districts, shouldn't be checked
        onView(withId(R.id.allD)).check(matches(not(isChecked())));
        onView(withId(R.id.fHainKberg)).check(matches(not(isChecked())));
        onView(withId(R.id.spandau)).check(matches(not(isChecked())));
    }
} //end of class