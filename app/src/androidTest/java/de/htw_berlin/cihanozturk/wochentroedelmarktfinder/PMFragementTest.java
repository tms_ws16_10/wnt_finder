package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.content.pm.ActivityInfo;
import android.test.ActivityInstrumentationTestCase2;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class PMFragementTest extends ActivityInstrumentationTestCase2<FilterFragment> {
    public PMFragementTest() {
        super(FilterFragment.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testAllCheckboxResult() {
        //turn into portrait(if is in landscape)
        //click on All Checkbox, should set all other checkboxes to true
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        onView(withId(R.id.allD)).perform(click());
        //check some Checkboxes
        onView(withId(R.id.bBurg)).check(matches(isChecked()));
        onView(withId(R.id.cBurgWilmers)).check(matches(isChecked()));
        onView(withId(R.id.rDorf)).check(matches(isChecked()));
        onView(withId(R.id.fHainKberg)).check(matches((isChecked())));
        onView(withId(R.id.spandau)).check(matches((isChecked())));
        onView(withId(R.id.nKölln)).check(matches(isChecked()));
        onView(withId(R.id.mitte)).check(matches(isChecked()));
        onView(withId(R.id.rDorf)).check(matches(isChecked()));
        onView(withId(R.id.lBerg)).check(matches((isChecked())));
        onView(withId(R.id.klaistow)).check(matches((isChecked())));
    }
} //end of class
