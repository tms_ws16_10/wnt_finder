package de.htw_berlin.cihanozturk.wochentroedelmarktfinder;

import android.test.ActivityInstrumentationTestCase2;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

//Test if btn_onlineMode is set to disable
public class HauptActivityTest extends ActivityInstrumentationTestCase2<HauptActivity> {
    public HauptActivityTest() {
        super(HauptActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testBtnForOnlineModeIsDisabled() {
        // When the btn_onlineMode is available,
        // check that it is disabled
        onView(withId(R.id.btn_onlineMode)).check(matches(not(isEnabled())));
    }
} //end of class
