# README #
# WochenUndTroedelmarktFinder
![Bildschirmfoto 2017-01-24 um 13.28.38.png](https://bitbucket.org/repo/zxBR8b/images/1959120399-Bildschirmfoto%202017-01-24%20um%2013.28.38.png)

**Inhalt**

Haben Sie es auch satt nach der Suche eines Wochen- oder Trödelmarktes die Öffnungszeiten, den Standort oder die Kontaktdaten im Internet zu Recherchieren.
Zumal Sie auch nicht den Name des Marktes kennen.
Der "Wochen- und Trödelmarkt Finder" beantwortet Ihnen alle fragen.
Sie wurde von engagierten Programmierern ins Leben gerufen. Man ist in der Lage diverse Informationen sich ausgeben zu lassen die da wären,
- Name- und Kontaktdaten eines Marktes
- Öffnungszeiten
- Standorte sich Live auf einer Karte auszugeben
- Zu einem Markt sich navigieren lassen

**Funktion**

Die Applikation bezieht seine Informationen über OPENDATA.
Sie werden beim ersten Start aufgefordert die "Filter"- Funktion zu benutzen, um zu entscheiden welche Objekte aus welchen Bezirken angezeigt werden soll.
Ohne dessen wird der "Map"-Button disabled bleiben.
Im Anschluss werden Ihnen die Standorte auf einer OSM ( OpenStreetMap ) Karte angezeigt durch farblich erkennbaren icons.

Ergänzend sind sie in der Lage die Vorhanden Wochen- und Trödelmarkt Objekte in der Liste (Liste-Button) anzeigen zu lassen.


**Anforderung** 
Die Applikation läuft auf alle Android-Fähigen Geräten.
API 23 Android 6.0 -> Target API 23 ->Min API 17

**Screenshots**

1. HauptScreen
2. Map
3. Filter
4. List
5. Credits

![Bildschirmfoto 2017-01-24 um 13.28.38.png](https://bitbucket.org/repo/zxBR8b/images/1959120399-Bildschirmfoto%202017-01-24%20um%2013.28.38.png)

![Bildschirmfoto 2017-01-24 um 13.28.26.png](https://bitbucket.org/repo/zxBR8b/images/3704754384-Bildschirmfoto%202017-01-24%20um%2013.28.26.png)

![Bildschirmfoto 2017-01-24 um 13.25.51.png](https://bitbucket.org/repo/zxBR8b/images/518236248-Bildschirmfoto%202017-01-24%20um%2013.25.51.png)

![Bildschirmfoto 2017-01-24 um 13.30.40.png](https://bitbucket.org/repo/zxBR8b/images/995243739-Bildschirmfoto%202017-01-24%20um%2013.30.40.png)

![Bildschirmfoto 2017-01-24 um 13.30.12.png](https://bitbucket.org/repo/zxBR8b/images/2044721785-Bildschirmfoto%202017-01-24%20um%2013.30.12.png)


**Ansprechpartner**

Team10WS16/17
Modul: Tech. mobiler Systeme
Programmierer:  Pujan & Cihan